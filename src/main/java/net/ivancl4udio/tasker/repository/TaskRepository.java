package net.ivancl4udio.tasker.repository;

import net.ivancl4udio.tasker.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TaskRepository extends MongoRepository<Task, String> {
    List<Task> findByTitleContaining(String title);
    List<Task> findByPublished(boolean published);
}
